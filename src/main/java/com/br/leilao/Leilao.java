package com.br.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public Leilao() {
        this.lances = new ArrayList<>();
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public List<Lance> adicionarLance(Lance lance) {
        int qtdLances = this.lances.size();

        if (qtdLances == 0)
            this.lances.add(lance);
        else if (lance.getValor() > this.lances.get(this.lances.size() - 1).getValor())
            this.lances.add(lance);
        else
            throw new RuntimeException("Não é permitido adicionar lance menor que o lance anterior.");

        return this.lances;
    }
}
