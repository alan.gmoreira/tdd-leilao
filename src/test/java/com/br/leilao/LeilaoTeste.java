package com.br.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {

    private Usuario usuario;
    private Leilao leilao;
    private Lance lance;
    private Lance lance2;
    private Lance lance3;

    @BeforeEach
    private void setUp() {
        usuario = new Usuario("Alan Garcia Moreira", 1);
        leilao = new Leilao();
        lance = new Lance(usuario, 100.00);
        lance2 = new Lance(usuario, 200.00);
        lance3 = new Lance(usuario, 50.00);
    }

    @Test
    public void adicionarPrimeiroNovoLance() {
        this.leilao.adicionarLance(this.lance);

        Assertions.assertEquals(this.lance, this.leilao.getLances().get(this.leilao.getLances().size() - 1));
    }

    @Test
    public void adicionarSegundoNovoLance() {
        this.leilao.adicionarLance(this.lance);
        this.leilao.adicionarLance(this.lance2);

        Assertions.assertEquals(this.lance, this.leilao.getLances().get(0));
        Assertions.assertEquals(this.lance2, this.leilao.getLances().get(this.leilao.getLances().size() - 1));
    }

    @Test
    public void adicionarLanceMenorQueUltimo() {
        this.leilao.adicionarLance(this.lance);

        Assertions.assertThrows(RuntimeException.class, () -> {this.leilao.adicionarLance(this.lance3);});
    }


}
